# MPXPP Client  Library

The MPXP Client Libraries are provided to provide easy, abstracted developer access to the functions exposed via the MPXPP API, including:

  - Login
  - Consumer Accounts and metadata (create, update, search)
  - Consumer Payments (create, cancel, list)
  - Consumer Payment Methods (create, edit, delete, list)
  - Consumer Payment Subscriptions aka. Recurring Payments (create, edit, delete, list, propose)
  - Notifications (send)
  - Documents (create, list by account id, search, download)
  

### Requirements

You will need: [Composer](https://getcomposer.org/download/) and [Git](https://git-scm.com/) installed in your environment.

### Installation

This library is designed to be installed via Composer; it should not be necessary for you to 'clone' this repository.

In your project directory, create a **composer.json** file as follows:

```
{
	"repositories": [
		{
			"type": "vcs",
			"url":  "git@bitbucket.org:mpxonline/mpxpp_client.git"
		}
	],
	"require": {
		"php": ">=5.4",
		"mpxonline/mpxpp_client": "1.*"
	}
}
```

Run the following command:

```sh
composer install
```

This will connect to the BitBucket repository and will download the libraries, placing them in the 'vendor' subdirectory. If the code does not download, you may need to request that your SSH public key be installed so you can access it.

See ```example.php``` for example usage of the library.

At any point in the future, you may upgrade the library to the latest version by running:

```sh
composer update
```


