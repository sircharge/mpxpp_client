<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';

// Init MPXPP client
$client = new \MPXPP\Client;
$client->username = 'mpx_assigned_username';
$client->password = 'mpx_assigned_password';
$client->instance = 'mpx_assigned_instance';

try {
	$client->loginValidate();
	echo "Successfully logged in.\n";
} catch (\MPXPP\ClientException $e) {
	echo "Failed to login: {$e->getMessage()}\n";
}
